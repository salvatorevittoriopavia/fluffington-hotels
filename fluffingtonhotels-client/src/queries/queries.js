import {gql} from 'apollo-boost'


const getUsers = gql`
	query{
		getUsers{
			id
			firstName
			lastName
			username
			password
			isAdmin
			transactions{
				total
				id

			}
		}
	}
`

const getUser = gql`
	query($id: String!){
		getUser(id: $id){
			id
			firstName
			lastName
			username
			password
			isAdmin
			transactions{
				total
				id
			}
		}
	}

`

const getTransaction = gql`
query($id:String!){
	getTransaction(id: $id){
		startingDate
		endingDate
		status
		total
		userId
		roomType
		id
	}
}`
const getTransactions = gql`
query{
	getTransactions{
		startingDate
		endingDate
		status
		total
		userId
		roomType
		id
	}
}`
const getUserTransactions = gql`
query($id:String!){
	getUserTransactions(id: $id){
		startingDate
		endingDate
		status
		total
		userId
		id
		roomType
	}
}`

const getPets = gql`
query{
	getPets{
		id
		petName
		petImage
	}
}
`

const getPet = gql`
query($id: String!){
	getPet{
		id
		petName
		petImage
	}
}
`
export {getPet, getPets, getUserTransactions, getUsers, getUser, getTransaction, getTransactions}