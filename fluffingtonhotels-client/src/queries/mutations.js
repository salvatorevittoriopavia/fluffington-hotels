import {gql} from 'apollo-boost'


const loginUser = gql`
mutation($username: String!, $password: String!) {
  loginUser(username:$username password:$password) {
    firstName
    lastName
    password
    isAdmin
    username
    id
  }
}`;

const createUser = gql`
mutation($firstName: String!, $lastName: String!, $username: String!, $password: String!, $isAdmin: Boolean!){
	createUser(firstName: $firstName, lastName: $lastName, username: $username, password: $password isAdmin: $isAdmin){
		firstName
		lastName
		password
		isAdmin
		username
	}
}
`;

const deleteUser = gql`
mutation($id: String!){
	deleteUser(id: $id)
}

`;
const deleteTransaction = gql`
mutation($id: String!){
  deleteTransaction(id: $id)
}

`;
const deletePet = gql`
mutation($id: String!){
	deletePet(id: $id)
}
`;

const updateUser = gql`
mutation($id: String!, $firstName: String!, $lastName: String!, $username: String!, $password: String!, $isAdmin: Boolean!){
	updateUser(id: $id, firstName: $firstName, lastName: $lastName, username: $username, password: $password isAdmin: $isAdmin){
		firstName
		lastName
		password
		isAdmin
		username
		password
	}
}
`;

const createTransaction = gql`
mutation($userId: String!, $startingDate: Date! $endingDate: Date! $total: Int! $status: String!, $roomType: String!){
  createTransaction(userId: $userId startingDate: $startingDate endingDate:$endingDate total: $total status: $status, roomType: $roomType ){
    id
    startingDate
    endingDate
    roomType
    status
  }
}
`

const updateTransaction = gql`
mutation($id: String!, $userId: String!, $startingDate: Date! $endingDate: Date! $total: Int! $status: String!, $roomType: String!) {
	updateTransaction(id: $id userId: $userId startingDate: $startingDate endingDate:$endingDate total: $total status: $status, roomType: $roomType){
    id
    startingDate
    endingDate
    roomType
    userId
    total
  }
}
`

const createPet = gql`
mutation($petName: String!, $petImage: String!){
  createPet(petName: $petName, petImage: $petImage){
    id
    petName
    petImage
  }
}
`
const updatePet = gql`
mutation($id: String!, $petName: String!, $petImage: String!){
  updatePet(id: $id, petName: $petName, petImage: $petImage){
    id
    petName
    petImage
  }
}
`

export {createPet, updatePet, deletePet, deleteUser, loginUser, createUser, updateUser, createTransaction, deleteTransaction, updateTransaction}