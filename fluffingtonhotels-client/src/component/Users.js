import React from "react";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { deleteUser } from "../queries/mutations.js";
import { getUsers } from "../queries/queries.js";
import { Container, Columns, Card, Button } from "react-bulma-components";
import Swal from "sweetalert2";
import {Link} from 'react-router-dom'

const Users = props => {
	const userData = props.getUsers.getUsers ? props.getUsers.getUsers : [];
	const deleteUserHandler = event => {
		let id = event.target.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteUser({
					variables: { id: id },
					refetchQueries: [
						{
							query: getUsers
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"The member has been deleted.",
					"success"
				);
			}
		});
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title className="OtherFont is-centered">
								User Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<tr>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Username</th>
											<th>Transactions</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										{userData.map(user => {
											let transactions = user
												.transactions
												? user.transactions
												: {};
											return (
												<tr key={user.id}>
													<td>{user.firstName}</td>
													<td>{user.lastName}</td>
													<td>{user.username}</td>
													<td>
														{transactions.map(transaction => {
															return transaction.id + " "
														})}
													</td>
													<td>
													<Link to={"/Users/Update/" + user.id}>
														<Button className="button is-fullwidth is-warning">
															Edit
														</Button>
													</Link>
														<Button
															color="danger"
															fullwidth
															onClick={
																deleteUserHandler
															}
															id={user.id}
														>
															Delete
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getUsers, { name: "getUsers" }),
	graphql(deleteUser, { name: "deleteUser" })
)(Users);
