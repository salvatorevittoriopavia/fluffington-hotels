import React, { useState } from "react";
import {
	Heading,
	Container,
	Columns,
	Card,
	Button,
	Tile
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { toBase64, nodeServer } from "../function.js";
import { getPets } from "../queries/queries.js";
import { createPet, deletePet } from "../queries/mutations.js";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

const GalleryAdmin = props => {
	console.log(props);
	const petData = props.getPets.getPets ? props.getPets.getPets : [];
	//hooks
	const [petName, setPetName] = useState("");
	const [petImage, setPetImage] = useState("");
	const fileRef = React.createRef();

	const petChangeHandler = event => {
		setPetName(event.target.value);
	};

	const petImageChangeHandler = e => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setPetImage(encodedFile);
		});
	};

	const deleteButtonHandler = event => {
		let id = event.target.id;

		Swal.fire({
			title: "Are you sure you want to cancel?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, I wish to delete!"
		}).then(result => {
			if (result.value) {
				let variables = {
					id: id
				};
				props.deletePet({
					variables: variables,
					refetchQueries: [
						{
							query: getPets
						}
					]
				});
				Swal.fire("Cancelled!", "Pet has been deleted.", "success");
			}
		});
	};

	const petFormSubmitHandler = event => {
		event.preventDefault();
		let variables = {
			petName: petName,
			petImage: petImage
		};
		props.createPet({
			variables: variables
		});

		Swal.fire("Pet Added!");
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>
								{" "}
								Add your pet!{" "}
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={petFormSubmitHandler}>
								<label>Pet Name</label>
								<input
									value={petName}
									onChange={petChangeHandler}
									type="text"
									className="input"
								/>
								<label>Pet Picture</label>
								<input
									onChange={petImageChangeHandler}
									type="file"
									accept="image/png"
									ref={fileRef}
								/>
								<Button className="is-primary">Submit</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					{petData.map(pet => {
						return (
							<Columns.Column size={3}>
								<Card>
									<Card.Image
										src={nodeServer() + pet.petImage}
									/>
									<Card.Content>{pet.petName}</Card.Content>
									<Link to={"/Gallery/Update/" + pet.id}>
										<Button className="is-warning">
											{" "}
											Edit{" "}
										</Button>
									</Link>
									<Button
										onClick={deleteButtonHandler}
										className="is-danger"
										id={pet.id}
									>
										{" "}
										Delete{" "}
									</Button>
								</Card>
							</Columns.Column>
						);
					})}
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(createPet, { name: "createPet" }),
	graphql(deletePet, { name: "deletePet" }),
	graphql(getPets, { name: "getPets" })
)(GalleryAdmin);
