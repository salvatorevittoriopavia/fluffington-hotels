import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import {flowRight as compose} from "lodash"
import {graphql} from "react-apollo"
import Swal from "sweetalert2"
import{loginUser} from "../queries/mutations.js"
import {Link} from "react-router-dom"



const LandingPage = props => {
	//hooks
	const [password, setPassword] = useState("");
	const [username, setUserName] = useState("");
	const userNameInputHandler = event => {
		setUserName(event.target.value);
	};
	const passwordInputHandler = event => {
		setPassword(event.target.value);
	};
	const formSubmitHandler = event => {
		event.preventDefault()
		let loginDetails = {
			username : username,
			password : password
		}
		props.loginUser({
			variables: loginDetails
		})
		.then(response => {
			let data = response.data.loginUser
			if(data===undefined) {
				Swal.fire({
				  icon: 'error',
				  title: 'Oops...',
				  text: 'Wrong Credentials!',
				})
			}else {
				// syntax: localStorage.setItem(key, value)
				localStorage.setItem('username', data.username)
				localStorage.setItem('isAdmin', data.isAdmin)
				localStorage.setItem('userId', data.id)
			}
		})
	}

	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card id="loginCard">
						<Card.Header>
							<Card.Header.Title className="is-centered OtherFont">
								Login
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<label htmlFor="userName"> Username </label>
								<input placeholder="input username here" id="userName" className="input" onChange={userNameInputHandler} value={username} />
								<label htmlFor="password"> Password</label>
								<input
									placeholder="input password here"
									type="password"
									onChange={passwordInputHandler}
									value={password}
									id="password"
									className="input"
								/>
								<Button type="submit" className="button is-fullwidth is-dark">Log In</Button>
							</form>
							<Link to="/SignUp">
								No account? Sign up!
							</Link>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(graphql(loginUser, {name: "loginUser"}))(LandingPage);