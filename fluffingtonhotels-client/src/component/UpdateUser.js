import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { updateUser } from "../queries/mutations.js";
import { getUser } from "../queries/queries.js";

const UpdateUser = props => {
	//hooks
	const [isAdmin, setIsAdmin] = useState('');
	const [password, setPassword] = useState("");
	const [username, setUserName] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [passwordConfirm, setPasswordConfirm] = useState("");
	const isAdminChangeHandler = event =>{
		setIsAdmin(event.target.value)
	}
	const userNameInputHandler = event => {
		setUserName(event.target.value);
	};
	const passwordInputHandler = event => {
		setPassword(event.target.value);
	};
	const firstNameChangeHandler = event => {
		setFirstName(event.target.value);
	};
	const lastNameChangeHandler = event => {
		setLastName(event.target.value);
	};
	const passwordConfirmInputHandler = event => {
		setPasswordConfirm(event.target.value);
	};
	let user = props.getUser.getUser ? props.getUser.getUser : {}
	console.log(props)
	if(!props.getUser.loading){
		const setDefault = () =>{
			setUserName(user.username)
			setFirstName(user.firstName)
			setLastName(user.lastName)
			setIsAdmin(user.isAdmin)
		}
		if(username == ''){
			setDefault()
		}
	}
	let isAdminHelper = true
	if(isAdmin === 'true'){
		isAdminHelper = true
	}else {
		isAdminHelper = false
	}

	const formSubmitHandler = event => {
		event.preventDefault();
		let variables = {
			id: props.match.params.id,
			firstName: firstName,
			lastName: lastName,
			username: username,
			password: password,
			isAdmin: isAdminHelper
		};
		if (password === passwordConfirm) {
			props.updateUser({
				variables: variables
			});
			Swal.fire("Welcome to the party!");
			setUserName("");
			setPassword("");
			setLastName("");
			setFirstName("");
		} else {
			Swal.fire("passwords do not match");
		}
	};
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card id="loginCard">
						<Card.Header>
							<Card.Header.Title className="is-centered OtherFont">
								Update User
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<label htmlFor="firstName"> First Name </label>
								<input
									placeholder="input first name here"
									id="firstName"
									className="input"
									onChange={firstNameChangeHandler}
									value={firstName}
								/>
								<label htmlFor="lastName"> Last Name </label>
								<input
									placeholder="input last name here"
									id="lastName"
									className="input"
									onChange={lastNameChangeHandler}
									value={lastName}
								/>
								<label htmlFor="userName"> Username </label>
								<input
									placeholder="input username here"
									id="userName"
									className="input"
									onChange={userNameInputHandler}
									value={username}
								/>
								<label htmlFor="password"> Password</label>
								<input
									placeholder="input password here"
									type="password"
									onChange={passwordInputHandler}
									value={password}
									id="password"
									className="input"
								/>
								<label htmlFor="passwordConfirm">
									Confirm Password
								</label>
								<input
									type="password"
									onChange={passwordConfirmInputHandler}
									id="passwordConfirm"
									className="input"
								/>
								<div className="select">
								<select value={isAdmin} onChange={isAdminChangeHandler}>
									<option value={true}>true</option>
									<option value={false}>false</option>
								</select>
								</div>
								<Button
									type="submit"
									className="button is-fullwidth is-dark"
								>
									Update
								</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(graphql(updateUser, { name: "updateUser" }), graphql(getUser, {options: props => {return {variables: {id :props.match.params.id}}}, name: "getUser"}))(UpdateUser)
