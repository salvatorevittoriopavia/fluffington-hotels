import React from "react";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { deleteTransaction } from "../queries/mutations.js";
import { getUserTransactions } from "../queries/queries.js";
import { Container, Columns, Card, Button } from "react-bulma-components";
import Swal from "sweetalert2";
import {Link} from 'react-router-dom'


const TransactionPage = props => {
	const transactionData = props.getUserTransactions.getUserTransactions ? props.getUserTransactions.getUserTransactions : [];
	const deleteTransactionHandler = event =>{
		let id = event.target.id;

		Swal.fire({
			title: "Are you sure you want to cancel?",
			text: "You won't be able to revert this!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, I wish to cancel!"
		}).then(result => {
			if (result.value) {
				props.deleteTransaction({
					variables: { id: id },
					refetchQueries: [
						{
							query: getUserTransactions
						}
					]
				});
				Swal.fire(
					"Cancelled!",
					"Your request has been cancelled.",
					"success"
				);
			}
		});
	}
	return (
		<Container>
			<Columns className="is-vcentered">
				<Columns.Column size={10}>
					<Card className="Cards">
						<Card.Header>
							<Card.Header.Title className="OtherFont is-centered">
								Transaction Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<tr>
											<th>Transaction ID</th>
											<th>Starting Date</th>
											<th>Ending Date</th>
											<th>Total</th>
											<th>Room Type</th>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										{transactionData.map(transaction => {
											return (
												<tr key={transaction.id}>
													<td>{transaction.id}</td>
													<td>{transaction.startingDate}</td>
													<td>{transaction.endingDate}</td>
													<td>{transaction.total}</td>
													<td>{transaction.roomType}</td>
													<td>{transaction.status}</td>
													<td>
														<Button
															color="danger"
															fullwidth
															onClick={
																deleteTransactionHandler
															}
															id={transaction.id}
														>
															Cancel
														</Button>
													</td>
												</tr>
											);
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
}

export default compose(graphql(deleteTransaction,{name: 'deleteTransaction'}), graphql(getUserTransactions, {options: props=>{return {variables: {id: localStorage.getItem('userId')}}}, name: "getUserTransactions"}))(TransactionPage)