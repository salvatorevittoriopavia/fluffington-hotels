import React, { useState } from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";

const NavBar = props => {
	console.log(localStorage.getItem('isAdmin'))
	//hooks
	const [open, setOpen] = useState(false);
	let adminLink = "";
	let customLink = "";
	if (localStorage.getItem('isAdmin') === 'true'){
		adminLink = (
			<Navbar color="dark">
			<Link className="navbar-item has-text-white is-dark" to="/Users">
			Users
			</Link>
			<Link className="navbar-item has-text-white is-dark" to="/transactionsAdmin">
			Transactions
			</Link>
			<Link className="navbar-item has-text-white is-dark" to="/GalleryAdmin">
			Gallery
			</Link>
			</Navbar>)
	}else if(localStorage.getItem('isAdmin') === 'false') {
		adminLink = (
			<Navbar>
			<Link className="navbar-item" to="/transactions">
			Transactions
			</Link>
			<Link className="navbar-item" to="/Gallery">
			Gallery
			</Link>
			</Navbar>
			)
	}
	if (localStorage.getItem('userId')) {
		customLink = (
			
			<Link to="/Logout" className="navbar-item">
				Logout
			</Link>
		);
	} else {
		customLink = (
			<Link to="/" className="navbar-item">
				Login
			</Link>
		);
	}
	return (
		<Navbar id="navBar" color="dark" active={open}>
			<Navbar.Brand>
				<Link to="/rooms" className="navbar-item">
					<h2 className="OtherFont">Fluffington Hotels</h2>
				</Link>

				<Navbar.Burger
					active={open}
					onClick={() => {
						setOpen(!open);
					}}
				/>
			</Navbar.Brand>
			<Navbar.Menu>
				<Navbar.Container position="end">
					<Link className="navbar-item" to="/rooms">
						Rooms
					</Link>
					{adminLink}
					{customLink}
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};

export default NavBar;
