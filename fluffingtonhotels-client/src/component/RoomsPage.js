import React, {useState} from "react";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { Container, Columns, Card, Button } from "react-bulma-components";
import Swal from "sweetalert2";
import {Link} from 'react-router-dom'
import {createTransaction} from '../queries/mutations.js'


const RoomsPage = props => {
	//hooks
	const [StartingDate1, setStartingDate1] = useState('');
	const [EndingDate1, setEndingDate1] = useState('');

	const [StartingDate2, setStartingDate2] = useState('');
	const [EndingDate2, setEndingDate2] = useState('');

	const [StartingDate3, setStartingDate3] = useState('');
	const [EndingDate3, setEndingDate3] = useState('');

	const startingDateChangeHandler1 = (event) =>{
		setStartingDate1(event.target.value)
	}
	const endingDateChangeHandler1 = (event) =>{
		setEndingDate1(event.target.value)
	}
	const startingDateChangeHandler2 = (event) =>{
		setStartingDate2(event.target.value)
	}
	const endingDateChangeHandler2 = (event) =>{
		setEndingDate2(event.target.value)
	}
	const startingDateChangeHandler3 = (event) =>{
		setStartingDate3(event.target.value)
	}
	const endingDateChangeHandler3 = (event) =>{
		setEndingDate3(event.target.value)
	}
	const formSubmitHandler1 = event =>{
		event.preventDefault()
		let variables = {
			userId: localStorage.getItem('userId'),
			startingDate: StartingDate1,
			endingDate: EndingDate1,
			total: 200,
			status: "pending approval",
			roomType: "Small Room"

		}
		props.createTransaction({
			variables: variables
		})
		Swal.fire('Success!')
	}
	const formSubmitHandler2 = event =>{
		event.preventDefault()
		let variables = {
			userId: localStorage.getItem('userId'),
			startingDate: StartingDate2,
			endingDate: EndingDate2,
			total: 400,
			status: "pending approval",
			roomType: "Large Room"
		}	
		props.createTransaction({
			variables: variables
		})
		Swal.fire('Success!')
	}
	const formSubmitHandler3 = event =>{
		event.preventDefault()
		let variables = {
			userId: localStorage.getItem('userId'),
			startingDate: StartingDate3,
			endingDate: EndingDate3,
			total: 300,
			status: "pending approval",
			roomType: "Exotic Room"
		}
		props.
		createTransaction({
			variables: variables
		})
		Swal.fire('Success!')
	}

	return(
	<Columns>
		<Columns.Column>
			<Card>
				<Card.Image size="4by3" src="/assets/room4.jpg"/>
				<Card.Header >
					<h1 className="is-centered OtherFont"> Small Animal Rooms </h1>
				</Card.Header>
				<Card.Content>
				<p> For your tiny friend </p>
				<form onSubmit={formSubmitHandler1}>
					<p>Starting date</p>
					<input className="input" value={StartingDate1} onChange={startingDateChangeHandler1} type="date" />
					<p>Ending date</p>
					<input className="input" value={EndingDate1} onChange={endingDateChangeHandler1} type="date" />
					<div className="is-right">
						<Button type="submit" className="is-link"> Rent! </Button>
					</div>
				</form>
				</Card.Content>
			</Card>
		</Columns.Column>
		<Columns.Column>
			<Card>
				<Card.Image size="4by3" src="/assets/room1.jpg"/>
				<Card.Header >
					<h1 className="has-text-centered OtherFont"> Large Animal Rooms </h1>
				</Card.Header>
				<Card.Content>
				<p> For your larger friend </p>
				<form onSubmit={formSubmitHandler2}>
					<p>Starting date</p>
					<input className="input" value={StartingDate2} onChange={startingDateChangeHandler2} type="date" />
					<p>Ending date</p>
					<input className="input" value={EndingDate2} onChange={endingDateChangeHandler2}  type="date" />
					<div className="is-right">
						<Button type="submit" className="is-link"> Rent! </Button>
					</div>
				</form>
				</Card.Content>
			</Card>
		</Columns.Column>
		<Columns.Column>
			<Card>
				<Card.Image size="4by3" src="/assets/room3.JPG"/>
				<Card.Header >
					<h1 className="has-text-centered OtherFont"> Exotic Animal Rooms </h1>
				</Card.Header>
				<Card.Content>
				<p> For your exotic friend </p>
				<form onSubmit={formSubmitHandler3}>
					<p>Starting date</p>
					<input className="input" value={StartingDate3} onChange={startingDateChangeHandler3} type="date" />
					<p>Ending date</p>
					<input className="input" value={EndingDate3} onChange={endingDateChangeHandler3}  type="date" />
					<div className="is-right">
						<Button type="submit" className="is-link"> Rent! </Button>
					</div>
				</form>
				</Card.Content>
			</Card>
		</Columns.Column>
	</Columns>)
}


export default compose(graphql(createTransaction, {name: 'createTransaction'}))(RoomsPage)