import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import {flowRight as compose} from "lodash"
import {graphql} from "react-apollo"
import Swal from "sweetalert2"
import{updatePet} from "../queries/mutations.js"
import{getPet} from "../queries/queries.js"
import {Link} from "react-router-dom"
import { toBase64, nodeServer } from "../function.js";



const UpdatePet = props => {
	console.log(props)
	const petData = props.getPet.getPet
	? props.getPet.getPet
	: [];
	//hooks
	const [petName, setPetName] = useState('')
	const [petImage, setPetImage] = useState('')
	const fileRef = React.createRef();


	const petChangeHandler = event => {
		setPetName(event.target.value)
	}

	const petImageChangeHandler = e => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setPetImage(encodedFile);
		});
	};
	if(!props.getPet.loading){
		const setDefault = () =>{
			setPetName(petData.petName)
		}
		if(petName === ''){
			setDefault()
		}
	}
	const petFormSubmitHandler = event =>{
		event.preventDefault()
		let variables = {
			petName: petName,
			petImage: petImage,
			id: props.match.params.id
		}
		props.updatePet({
			variables: variables
		})

		Swal.fire('Pet Updated!')
	}


	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card id="loginCard">
						<Card.Header>
							<Card.Header.Title className="is-centered OtherFont">
								Update Pet!
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={petFormSubmitHandler}>
								<label htmlFor="userName"> Pet Name </label>
								<input id="userName" className="input" onChange={petChangeHandler} value={petName} />
								<label>Profile Picture</label>
								<input onChange={petImageChangeHandler} type="file" accept="image/png" ref={fileRef} />
								<Button type="submit" className="button is-fullwidth is-dark">Update</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(graphql(updatePet, {name: "updatePet"}), graphql(getPet, {options: props=>{return {variables: {id: props.match.params.id}}}, name: "getPet"}))(UpdatePet);