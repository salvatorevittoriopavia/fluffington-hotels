import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import { flowRight as compose } from "lodash";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { updateTransaction } from "../queries/mutations.js";
import { getTransaction } from "../queries/queries.js";


const UpdateTransaction = props => {
	//hooks
	const transactionData = props.getTransaction.getTransaction ? props.getTransaction.getTransaction : [];
	const [startingDate, setStartingDate] = useState('')
	const [endingDate, setEndingDate] = useState('')
	const [status, setStatus] = useState('')
	const [roomType, setRoomType] = useState('')

	const startingDateChangeHandler = event =>{
		setStartingDate(event.target.value)
	}
	const endingDateChangeHandler = event =>{
		setEndingDate(event.target.value)
	}
	const statusChangeHandler = event =>{
		setStatus(event.target.value)
	}
	const roomTypeChangeHandler = event =>{
		setRoomType(event.target.value)
	}
	if(!props.getTransaction.loading){
		const setDefault = () =>{
			setStartingDate(transactionData.startingDate)
			setEndingDate(transactionData.endingDate)
			setStatus(transactionData.status)
			setRoomType(transactionData.roomType)
		}
		if(roomType == ''){
			setDefault()
		}
	}



	const formSubmitHandler = event => {
		let total = 0
		if(roomType == 'Small Room'){
			total = 200
		}
		if(roomType == 'Large Room'){
			total = 400
		}
		if(roomType == 'Exotic Room'){
			total = 300
		}
		event.preventDefault()
		let variables = {
			id: props.match.params.id,
			startingDate: startingDate,
			endingDate: endingDate,
			total: total,
			userId: transactionData.userId,
			status: status,
			roomType: roomType
		}
		props.updateTransaction({
			variables: variables
		})
		Swal.fire('Success!')
	}
	return(
		<Container>
			<Columns>
				<Columns.Column size={12}>
					<Card>
						<Card.Header>
							<Card.Header.Title className="OtherFont is-centered">
								Transaction Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
							<form onSubmit={formSubmitHandler} className="has-text-centered">
								<table className="table is-fullwidth is-bordered">
									<thead>
										<tr>
											<th>Transaction ID</th>
											<th>Starting Date</th>
											<th>Ending Date</th>
											<th>Status</th>
											<th>Room Type</th>
										</tr>
									</thead>
									<tbody>
											<tr>
												<td>{transactionData.id}</td>
												<td><input onChange={startingDateChangeHandler} className="input" type="date" value={startingDate}/></td>
												<td><input onChange={endingDateChangeHandler} className="input" type="date" value={endingDate}/></td>
												
												<td><div className="select"><select value={status} onChange={statusChangeHandler}>
													<option>Pending</option>
													<option>Completed</option>
													<option>Active</option>
												</select>	</div></td>

												<div className="select">
												<td><select value={roomType} onChange={roomTypeChangeHandler}>
													<option>Small Room</option>
													<option>Large Room</option>
													<option>Exotic Room</option>
												</select></td>
												</div>
											</tr>
									</tbody>
								</table>
								<Button className="is-centered is-link"> Submit Changes </Button>
								</form>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
}

export default compose(graphql(updateTransaction,{name: 'updateTransaction'}), graphql(getTransaction, {options: props=>{return {variables: {id: props.match.params.id}}}, name: "getTransaction"}))(UpdateTransaction)