import React, { useState } from "react";
import {Box, Container, Columns, Card, Button } from "react-bulma-components";
import {flowRight as compose} from "lodash"
import {graphql} from "react-apollo"
import Swal from "sweetalert2"
import{loginUser} from "../queries/mutations.js"
import {Link} from "react-router-dom"


const Logout = props => {
	const logoutFunction = event =>{
		localStorage.clear()
	}
	return (
		<Box className="has-text-centered"> 
			<p>Are you sure you want to log out?</p>
			<form> 
			<Button type="submit" className="is-danger" onClick={logoutFunction}> Yes </Button>
			</form>
		</Box>


		)
}

export default Logout