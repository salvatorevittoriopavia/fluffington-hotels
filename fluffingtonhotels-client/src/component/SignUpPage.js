import React, { useState } from "react";
import { Container, Columns, Card, Button } from "react-bulma-components";
import {flowRight as compose} from "lodash"
import {graphql} from "react-apollo"
import Swal from "sweetalert2"
import{createUser} from "../queries/mutations.js"



const SignUpPage = props => {
	//hooks
	const [password, setPassword] = useState("");
	const [username, setUserName] = useState("");
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [passwordConfirm, setPasswordConfirm] = useState('')
	const userNameInputHandler = event => {
		setUserName(event.target.value);
	};
	const passwordInputHandler = event => {
		setPassword(event.target.value);
	};
	const firstNameChangeHandler = event =>{
		setFirstName(event.target.value)
	}
	const lastNameChangeHandler = event =>{
		setLastName(event.target.value)
	}
	const passwordConfirmInputHandler = event =>{
		setPasswordConfirm(event.target.value)
	}
	const formSubmitHandler = event => {
		event.preventDefault()
		let variables = {
			firstName: firstName,
			lastName: lastName,
			username: username,
			password: password,
			isAdmin: false
		}
		if(password === passwordConfirm){


		props.createUser({
			variables: variables,
		})
		Swal.fire(
			'Welcome to the party!')
		setUserName('')
		setPassword('')
		setLastName('')
		setFirstName('')
		
	}else{
		Swal.fire('passwords do not match')
	}
}
	return (
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card id="loginCard">
						<Card.Header>
							<Card.Header.Title className="is-centered OtherFont">
								Login
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={formSubmitHandler}>
								<label htmlFor="firstName"> First Name </label>
								<input placeholder="input first name here" id="firstName" className="input" onChange={firstNameChangeHandler} value={firstName}/>
								<label htmlFor="lastName"> Last Name </label>
								<input placeholder="input last name here" id="lastName" className="input" onChange={lastNameChangeHandler} value={lastName}/>
								<label htmlFor="userName"> Username </label>
								<input placeholder="input username here" id="userName" className="input" onChange={userNameInputHandler} value={username} />
								<label htmlFor="password"> Password</label>
								<input
									placeholder="input password here"
									type="password"
									onChange={passwordInputHandler}
									value={password}
									id="password"
									className="input"
								/>
								<label htmlFor="passwordConfirm">Confirm Password</label>
								<input
									
									type="password"
									onChange={passwordConfirmInputHandler}
									id="passwordConfirm"
									className="input"
								/>

								<Button type="submit" className="button is-fullwidth is-dark">Sign Up</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(graphql(createUser, {name: "createUser"}))(SignUpPage);