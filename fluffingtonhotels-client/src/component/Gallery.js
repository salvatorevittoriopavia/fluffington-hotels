import React, { useState } from "react";
import {
	Heading,
	Container,
	Columns,
	Card,
	Button,
	Tile
} from "react-bulma-components";
import { graphql } from "react-apollo";
import { flowRight as compose } from "lodash";
import { toBase64, nodeServer } from "../function.js";
import { getPets } from "../queries/queries.js";
import { createPet } from "../queries/mutations.js";
import Swal from "sweetalert2";

const Gallery = props => {
	const petData = props.getPets.getPets ? props.getPets.getPets : [];
	//hooks
	const [petName, setPetName] = useState("");
	const [petImage, setPetImage] = useState("");
	const fileRef = React.createRef();

	const petChangeHandler = event => {
		setPetName(event.target.value);
	};

	const petImageChangeHandler = e => {
		// console.log(fileRef.current.files[0]);
		toBase64(fileRef.current.files[0]).then(encodedFile => {
			setPetImage(encodedFile);
		});
	};

	const petFormSubmitHandler = event => {
		event.preventDefault();
		let variables = {
			petName: petName,
			petImage: petImage
		};
		props.createPet({
			variables: variables
		});

		Swal.fire("Pet Added!");
	};

	return (
		<Container>
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title className="OtherFont">
								{" "}
								Add your pet!{" "}
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={petFormSubmitHandler}>
								<label>Pet Name</label>
								<input
									value={petName}
									onChange={petChangeHandler}
									type="text"
									className="input"
								/>
								<label>Pet Picture</label>
								<input
									onChange={petImageChangeHandler}
									type="file"
									accept="image/png"
									ref={fileRef}
								/>
								<Button className="is-primary">Submit</Button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
				<Columns.Column size={9}>
					{petData.map(pet => {
						return (
							<Columns.Column size={3}>
								<Card>
									<Card.Image
										src={nodeServer() + pet.petImage}
									/>
									<Card.Content>{pet.petName}</Card.Content>
								</Card>
							</Columns.Column>
						);
					})}
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(createPet, { name: "createPet" }),
	graphql(getPets, { name: "getPets" })
)(Gallery);
