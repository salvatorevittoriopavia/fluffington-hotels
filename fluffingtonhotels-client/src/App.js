import React, { useState } from "react";
import "react-bulma-components/dist/react-bulma-components.min.css";
import "./App.css";
import { Box, Button } from "react-bulma-components";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ApolloClient from 'apollo-boost'
import {ApolloProvider} from "react-apollo"

//components
import SignUpPage from "./component/SignUpPage.js"
import LandingPage from './component/LandingPage.js'
import NavBar from './component/NavBar.js'
import Users from './component/Users.js'
import UpdateUser from './component/UpdateUser.js'
import RoomsPage from './component/RoomsPage.js'
import TransactionsPage from './component/TransactionsPage'
import UpdateTransaction from './component/UpdateTransaction.js'
import Logout from './component/Logout.js'
import TransactionsAdminPage from './component/TransactionsAdmin'
import Gallery from './component/Gallery.js'
import GalleryAdmin from './component/GalleryAdmin.js'
import UpdatePet from "./component/UpdatePet.js"

const client = new ApolloClient({uri: "http://localhost:4000/batch43"})


function App() {
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
    	<NavBar />
        <Switch>
          <Route exact path="/" component={LandingPage} />
          <Route path="/SignUp" component={SignUpPage} />
          <Route exact path="/Users" component={Users} />
          <Route exact path="/Users/Update/:id" component={UpdateUser} />
          <Route exact path="/Gallery/Update/:id" component={UpdatePet} />
          <Route exact path="/rooms" component={RoomsPage} />
          <Route exact path="/transactions/" component={TransactionsPage} />
          <Route exact path="/transactionsAdmin/" component={TransactionsAdminPage} />
          <Route exact path="/transactions/Update/:id" component={UpdateTransaction} />
          <Route exact path="/Logout" component={Logout} />
          <Route exact path="/Gallery" component={Gallery} />
          <Route exact path="/GalleryAdmin" component={GalleryAdmin} />
        </Switch>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
