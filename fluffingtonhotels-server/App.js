const express = require('express');
//instantiate an express project
const app = express();
const port = 4000;
const mongoose = require('mongoose');

const bodyParser = require("body-parser")

mongoose.connect("mongodb+srv://salvatorevittoriopavia:password12345@nosql-l9mhs.mongodb.net/test?retryWrites=true&w=majority", { useNewUrlParser: true, useCreateIndex: true, })
//check if connection succeeds
mongoose.connection.once("open", ()=>{
	console.log("Now connected to the MongoDB server!!!")
})

// increase the limit of uploaded files
app.use(bodyParser.json({ limit: "15mb" }))

// allow users to access a folder in the server by serving the static data
// syntax: app.use("/path", express.static("foldertoserve"))
app.use('/images', express.static('images'))
//import the instantiation of the apollo server
const server = require('./queries/queries.js')

//the app will be served by the apollo server instead of express
server.applyMiddleware({
	app,
	path: "/batch43"
})

app.listen(port, () =>{
	console.log(`🚀  Server ready at http://localhost:4000${server.graphqlPath}`);
});