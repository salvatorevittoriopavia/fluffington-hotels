const mongoose = require('mongoose')
const Schema = mongoose.Schema

const petSchema = new Schema({
	petName:{
		type: String,
		required: true
	},
	petImage:{
		type: String,
		required: true
	}
})

module.exports = mongoose.model('Pet', petSchema)