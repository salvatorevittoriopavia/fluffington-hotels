const mongoose = require('mongoose')
const Schema = mongoose.Schema
const graphQLDate = require('graphql-iso-date')

const transactionSchema = new Schema({
	startingDate: {
		type: graphQLDate,
		required: true
	},
	endingDate:{
		type: graphQLDate,
		required: true
	},
	userId: {
		type: String,
		required: true
	},
	total:{
		type: Number,
		required: true
	},
	status: {
		type: String,
		required: true
	},
	roomType:{
		type: String,
		required: true
	}
})

module.exports = mongoose.model('Transaction', transactionSchema)