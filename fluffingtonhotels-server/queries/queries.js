const {ApolloServer, gql} =require('apollo-server-express');
const {graphQLDateTime} = require('graphql-iso-date')
const bcrypt = require("bcrypt")
const uuid = require('uuid/v1')
const fs = require('fs');
//mongoose models
const User = require('../models/User.js');
const Transaction = require('../models/Transaction.js')
const Pet = require('../models/Pet.js')

//CRUD
//Type Query == Retrieve/Read
//Type Mutation == Create/Update/Delete
const customScalarResolver = {
	Date: graphQLDateTime
}
const typeDefs = gql`
	scalar Date

	type PetType{
		id: ID
		petName: String!
		petImage: String!
	}

	type UserType{
		id: ID
		firstName: String!
		lastName: String!
		username: String!
		password: String!
		isAdmin: Boolean!
		transactions: [TransactionType]
	}

	type TransactionType{
		id: ID
		startingDate: Date!
		endingDate: Date!
		userId: String!
		total: Int!
		status: String!
		roomType: String!
	}

	type Query{
		getPets: [PetType]
		getPet(id: String!): PetType
		getUsers: [UserType]
		getUser(id: String!) : UserType
		getTransactions: [TransactionType]	
		getTransaction(id: String!) : TransactionType
		getUserTransactions(id: String!) : [TransactionType]

	}

	type Mutation{

		createPet(
		petName: String!
		petImage: String!
		): PetType

		updatePet(
		id: String!
		petName: String!
		petImage: String
		): PetType

		createTransaction(
		startingDate: Date!
		endingDate: Date!
		userId: String!
		total: Int!
		status: String!
		roomType: String!
		): TransactionType

		createUser(
		firstName: String!
		lastName: String!
		username: String!
		password: String!
		isAdmin: Boolean!
		): UserType

		updateTransaction(
		id: String!
		startingDate: Date!
		endingDate: Date!
		status: String!
		userId: String!
		total: Int!
		roomType: String!
		): TransactionType

		updateUser(
		id: String!
		firstName: String!
		lastName: String!
		username: String!
		password: String!
		isAdmin: Boolean!
		): UserType

		deleteUser(
		id:String!): Boolean

		deleteTransaction(
		id: String!): Boolean

		deletePet(
		id: String!): Boolean

		loginUser(
		username: String!
		password: String!
		): UserType
	}
`

const resolvers = {
	Query: {
		getPets: () =>{
			return Pet.find({})
		},
		getPet:(parent, args) =>{
			return Pet.findOne({_id: args.id})
		},
		getTransactions: () =>{
			return Transaction.find({})
		},
		getUsers:()=>{
			return User.find({})
		},
		getUserTransactions: (parent, args) =>{
			return Transaction.find({userId: args.id})
		},
		getTransaction: (parent, args) =>{
			return Transaction.findOne({_id: args.id})
		},
		getUser: (parent, args) =>{
			return User.findOne({_id: args.id})
		}
	},
	Mutation: {
		createPet: (parents,args) => {
			let imageString = args.petImage
			let imageBase = imageString.split(';base64,').pop()
			let imageLocation="images/" +uuid()+ ".png";	
			// syntax fs.writeFile(fileName/descriptor, data, options, callback function)
			fs.writeFile(imageLocation, imageBase, {encoding: "base64"}, err=>{})

			let newPet = Pet({

				petName: args.petName,
				petImage: imageLocation
			})

			return newPet.save()
		},
		updatePet: (parents,args) => {
			let imageString = args.petImage
			let imageBase = imageString.split(';base64,').pop()
			let imageLocation="images/" +uuid()+ ".png";	
			// syntax fs.writeFile(fileName/descriptor, data, options, callback function)
			fs.writeFile(imageLocation, imageBase, {encoding: "base64"}, err=>{})

			return Pet.findOneAndUpdate({_id: args.id}, 
			{

				petName: args.petName,
				petImage: imageLocation
			})
		},
		createTransaction : (parents,args)=>{
			let newTransaction = Transaction({
				startingDate: args.startingDate,
				endingDate: args.endingDate,
				userId: args.userId,
				total: args.total,
				status: args.status,
				roomType : args.roomType
			})

			return newTransaction.save()
		},
		createUser : (parents, args) =>{
			let newUser = User({
				firstName: args.firstName,
				lastName: args.lastName,
				username: args.username,
				password: bcrypt.hashSync(args.password, 10),
				isAdmin: args.isAdmin
			})
			return newUser.save()
		},
		updateTransaction: (parents,args) => {
			return Transaction.findOneAndUpdate({_id: args.id}, 
				{
					startingDate: args.startingDate, 
					endingDate: args.endingDate, 
					userId: args.userId, 
					total: args.total, 
					status: args.status,
					roomType: args.roomType
				}, {new: true})
		},
		updateUser: (parents,args) => {
			return User.findOneAndUpdate({_id: args.id},
			{
				firstName: args.firstName, 
				lastName: args.lastName, 
				username: args.username, 
				password: bcrypt.hashSync(args.password, 10), 
				isAdmin: args.isAdmin
			}, {new:true})
		},
		deleteUser: (parents, args) => {
			let condition = args.id
			return User.findByIdAndDelete(condition).then((member, err)=>{
				if(err || !member){
					console.log("Delete Failed")
					return false
				}else{
					console.log("user deleted")
					return true
				}
			})
		},
		deleteTransaction: (parents, args) => {
			let condition = args.id
			return Transaction.findByIdAndDelete(condition).then((member, err)=>{
				if(err || !member){
					console.log("Delete Failed")
					return false
				}else{
					console.log("Transaction deleted")
					return true
				}
			})
		},
		deletePet: (parents, args) => {
			let condition = args.id
			return Pet.findByIdAndDelete(condition).then((pet, err)=>{
				if(err || !pet){
					console.log("Delete Failed")
					return false
				}else{
					console.log("Pet deleted")
					return true
				}
			})
		},
		loginUser: (parents,args)=> {
			return User.findOne({username: args.username})
			.then(user=>{
				if(user=== null){
					console.log("Username not found")
					return null
				}
				let hashedPassword = bcrypt.compareSync(args.password, user.password)
				if(hashedPassword === false){
					console.log('wrong password')
				}else{
					return user
				}
			})
		}
	},

	UserType : {
		transactions: (parents, args) => {
			return Transaction.find({userId: parents.id})
		}
	}
}


const server = new ApolloServer({
	typeDefs,
	resolvers
})

module.exports = server